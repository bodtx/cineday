import css from './cinedayAuto.css';
import CryptoJS from 'crypto-js';
require("./icons/Popcorn_icon.png");

function saveOptions(e) {
    e.preventDefault();
    console.log(CryptoJS.AES.encrypt(document.querySelector("#pass").value, 'vive le cinaday').toString());
    browser.storage.local.set({
        login: document.querySelector("#login").value,
        pass: CryptoJS.AES.encrypt(document.querySelector("#pass").value, 'vive le cinaday').toString(),
        mail: document.querySelector("#mail").value
    }).then(() => {
        document.querySelector("#save").classList.add("auVert");
    });


}

function restoreOptions() {

    browser.storage.local.get("login").then((result) => document.querySelector("#login").value = result.login || "", (error) => console.error(`Error: ${error}`));
    browser.storage.local.get("pass").then((result) => document.querySelector("#pass").value = CryptoJS.AES.decrypt(result.pass, 'vive le cinaday').toString(CryptoJS.enc.Utf8) || "", (error) => console.error(`Error: ${error}`));
    browser.storage.local.get("mail").then((result) => document.querySelector("#mail").value = result.mail || "", (error) => console.error(`Error: ${error}`));
}


document.querySelector("#save").addEventListener("animationend", function() {
    document.querySelector("#save").classList.remove("auVert");
})
document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);