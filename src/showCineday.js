try {

    if (document.getElementsByClassName("orderButtonClass").item(0) === undefined) {
        throw "c'est pas l'heure du cineday";
    } else {
        document.getElementsByClassName("orderButtonClass").item(0).click();
    }

    setTimeout(() => {
        browser.storage.local.get("mail").then((result) => {
            document.querySelector("#champsMail").value = result.mail || "";
            document.getElementsByClassName("submitButtonSendForm").item(1).click();
            browser.runtime.sendMessage({
                texte: "Cineday récupéré 😍"
            });
        }, (error) => console.error(`Error: ${error}`));
    }, 5000);
} catch (error) {
    browser.runtime.sendMessage({
        texte: "Erreur de récupération cineday"
    });
    console.error(error);
}