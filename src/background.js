function checkTime() {

    let date = new Date();
    let hours = date.getHours();
    var cinetabId;

    if ((hours == 10) && (date.getDay() == 2)) {
        console.log("toto");
        var cinedayTab = browser.tabs.create({
            url: "https://id.orange.fr/auth_user/bin/auth_user.cgi?cas=nowg&return_url=http%3A%2F%2Fmdsp.orange.fr%2Fcineday%2Fcommande%2FloadingWebPage",
            active: false
        }).then(tab => {
            browser.tabs.executeScript(tab.id, {
                file: "loginOrange.js"
            });
            cinetabId = tab.id;
            browser.tabs.onUpdated.addListener(handleUpdated);
        });

        function handleUpdated(tabId, changeInfo, tabInfo) {
            console.log(changeInfo);
            if (cinetabId == tabInfo.id && tabInfo.url.indexOf("password")!==-1 || tabInfo.url.indexOf("keep-connected")!==-1 && tabInfo.status == "complete") {
                console.log("Tab: " + tabId +
                    " URL changed to " + tabInfo.url + " and " + tabInfo.status);
                browser.tabs.executeScript(tabId, {
                    file: "passOrange.js"
                });

            }else if (cinetabId == tabInfo.id && tabInfo.url == "http://mdsp.orange.fr/cineday/commande/loadingWebPage" && tabInfo.status == "complete") {
                browser.tabs.onUpdated.removeListener(handleUpdated);
                console.log("Tab: " + tabId +
                    " URL changed to " + tabInfo.url + " and " + tabInfo.status);
                browser.tabs.executeScript(tabId, {
                    file: "showCineday.js"
                });

            }
        }

    }
}

browser.runtime.onMessage.addListener(message =>
    browser.notifications.create("cinedayAuto", {
        "type": "basic",
        "iconUrl": browser.extension.getURL("icons/Popcorn_icon.png"),
        "title": "Cineday",
        "message": message.texte
    })

);

checkTime();

// Set up an alarm to check this regularly.
browser.alarms.onAlarm.addListener(checkTime);
browser.alarms.create('checkTime', {
    periodInMinutes: 60
});