
const path = require('path');
const uglify = require('uglifyjs-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const  HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const copy = require('copy-webpack-plugin');

const dev = process.env.NODE_ENV === "dev";


    
let config =    {
  entry: {
   config: './src/config.js',   
  },
    watch: dev,
    output:{
    path: path.resolve(__dirname, 'dist'),
    filename: dev  ? '[name].js' : '[name].[chunkhash].js'
  },
    devtool: dev ? "inline-cheap-module-source-map" : "source-map",
    module: {
   rules: [
       {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
{
          loader: "eslint-loader",
        options: {
emitWarning: true
        }
}
        ],
      },
 
      {  
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader']
      },
       {
        test: /\.(png|jpg|gif)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 5182
            }
          }
        ]
      }
            
    ]
  },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'config.html',
      template: 'src/config.html',
             chunks: ['config']
        }),
    new CleanWebpackPlugin(['dist/*.*','dist/icons'],{
  root:     __dirname,
  verbose:  true,
  dry:      false
}),
        new copy([{
             from: 'src/manifest.json' 
        },
                 {
             from: 'src/icons/Popcorn_icon.png', to: 'icons/Popcorn_icon.png'
        }
                 ])
  
    ]
  

};

if(!dev){
    config.plugins.push(new uglify({
        sourceMap:true
    }));
}

let pageActions =    {
  entry: {
      loginOrange: './src/loginOrange.js',
      passOrange: './src/passOrange.js',
      background: './src/background.js',
      showCineday: './src/showCineday.js'
  },
    watch: dev,
    output:{
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js'
  },
    devtool: dev ? "inline-cheap-module-source-map" : "source-map",
  

};

module.exports = [config,pageActions];    

